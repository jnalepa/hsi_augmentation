# Training- and Test-Time Data Augmentation for Hyperspectral Image Segmentation
## Jakub Nalepa, Member, IEEE, Michal Myller, and Michal Kawulok, Member, IEEE

This repository provides supplementary material to the paper listed above (submitted to the IEEE Geoscience and Remote Sensing Letters journal). It includes:
- Example visualizations (*HSI augmentation - visualizations*) of the segmentation results obtained using the 1D- and 3D-convolutional neural networks with and without hyperspectral data augmentation.

- For each dataset (salinas, indiana, and pavia which correspond to Salinas Valley, Indian Pines, and Pavia University, respectively), the subfolders include:
    - *3D-CNN*&mdash;example visualizations obtained using the 3D-CNN model trained over original and augmented datasets.
    - *Training-time augmentation*&mdash;example visualizations obtained using 1D-CNN trained over original and augmented datasets (using training-time augmentation techniques).
    - *Test-time and mixed augmentation*&mdash;example visualizations obtained using 1D-CNN trained over original and augmented datasets (using test-time and mixed augmentation techniques).
- The detailed experimental results (*2019_HSI_data_augmentation_detailed_results.pdf*) which include:
    - Per-class classification accuracy for all hyperspectral datasets and experimental scenarios.
    - The results of all Wilcoxon's statistical tests.